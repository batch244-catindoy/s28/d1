// CRUD Operations
// C - Create, R - Retrieve, U- Update, D - Delete
// Crud Operations are the heart of any backend application

// [SECTION] Inserting Documents (Create)
		// Syntax - db.collectionName.insertOne({object});

	db.users.insertOne({
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact: {
			phone: "987654321",
			email: "janedoe@gmail.com",
		},
		courses: ["CSS", "Javascript", "Phyton"],
		department: "none"
	});
	
	db.users.insertOne({
		firstName: "Jerome",
		lastName: "Del Rio",
		age: "17",
		contact: {
			phone: "987654321",
			email: "jeromedelrio22@gmail.com",
		},
		courses: ["CSS", "Javascript", "Phyton"],
		batch: "244"
	});

	// Inserting Many Documents
	db.users.insertMany([
			{
				firstName: "Stephen",
				lastName: "Hawking",
				age: 76,
				contact: {
					phone: "987654321",
					email: "stephenhawking@gmail.com"
				},
				courses: ["Phyton", "React", "Php"],
				department: "none"
			},
			{
				firstName: "Neil",
				lastName: "Armstrong",
				age: 98,
				contact: {
					phone: "987654321",
					email: "neilarmstrong@gmail.com"
				},
				courses: ["React", "Phyton", "Sass"],
				department: "none"
			}
		]);

// [SECTION] Finding documents (Read/Retrieve)
		// syntax: db.collectionName.find()
		// will retrive all the documents in the database
	db.users.find();

	//Finding Documents with one field
	db.users.find({firstName: "Stephen"});

	//Finding Documents with multiple parameters
	db.users.find({lastName: "Armstrong", age: 98});

//[SECTION] Updating documents (Update)

	db.users.insert({
		firstName: "Test",
				lastName: "Test",
				age: 0,
				contact: {
					phone: "00000000",
					email: "test@gmail.com"
				},
				courses: [],
				department: "none"
	});

	// Updating a single document
	/*Syntax: db.collectionName.updateOne({criteria},
	{$set: {field: value}})*/

	db.users.updateOne({firstName: "Test"},
	{
		$set : {
			firstName: "Bill",
			lastName: "Gates",
			age : 65,
			contact: {
				phone: "123456789",
				email: "bill@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}
);

	//  Updating multiple documents

	db.users.updateMany({department: "none"},
			{
				$set: {
					department: "HR"
				}
			}
		);

	db.users.replaceOne({firstName: "Bill"},

			{
				firstName: "Test",
			lastName: "Test",
			age : 0,
			contact: {
				phone: "123456789",
				email: "test@gmail.com"
			},
			courses: [],
			}
		);

// [SECTION] Deleting documents (Delete)
	// Deleting single/multiple
	// Syntax: db.collectionName.deleteOne({criteria})

db.users.deleteOne({firstName: "Test"});


	// Advance queries

	// Query an embedded document
	db.users.find({
		contact: {
			phone: "987654321",
			email: "stephenhawking@gmail.com"
		}
	});

	// Query at nested field
	db.users.find({
		"contact.email": "janedoe@gmail.com"
	});

	//Querying an array without regard to order

	db.users.find({courses: {$all: ["React", "Phyton"]}});

	//Querying an array with excat elements

	db.users.find({courses: ["CSS", "Javascript", "Phyton"]});

	// Querying an embedded array

	db.users.insert({
		namearr: [
					{
						namea: "Juan"
					},
					{
						nameb: "Tamad"
					}	
				]
	})

	db.users.find({
		namearr:
				{
					namea: "Juan"
				}
	})